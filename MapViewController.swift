//
//  MapViewController.swift
//  PersonTabGit
//
//  Created by Joao Fernandes on 06/10/16.
//  Copyright © 2016 Joao Fernandes. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class MapViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    let locationManager = CLLocationManager()
    
    
    @IBOutlet weak var mapView: MKMapView!
    
    var annotation = MKPointAnnotation()
    
    var location:CLLocationCoordinate2D? // é nil
    var loca = CLLocationCoordinate2D() // retorna (0,0)
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.distanceFilter = kCLDistanceFilterNone // quanto é que a localização tem que mudar
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
            
            mapView.delegate = self
            mapView.showsUserLocation = true
            
            

        } else {
            //inform user
        }
        
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Erro \(error)")
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if status  == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
        
        
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locationObj = locations.last
        let coord = locationObj?.coordinate
        
        if let c = coord {
            print ( "latitude: \(c.latitude) longitude: \(c.longitude)")
            
            let span = MKCoordinateSpanMake(0.1, 0.1)
            let region = MKCoordinateRegionMake(c, span)
            mapView.setRegion(region, animated: true)
            annotation.coordinate = c
            annotation.title = "User Location"
            annotation.subtitle = "Here"
            
            mapView.addAnnotation(annotation)
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    
    @IBAction func didTapMap(_ sender: UITapGestureRecognizer) {
        let tapAnnotation = MKPointAnnotation()
        let tapPoint = sender.location(in: mapView)
        location = mapView.convert(tapPoint, toCoordinateFrom: mapView)
        
       // mapView.removeAnnotation(tapAnnotation)
        
        tapAnnotation.coordinate = location!
        tapAnnotation.title = "Tapped location"
        mapView.addAnnotation(tapAnnotation)
    }
    


    
}
