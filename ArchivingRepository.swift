//
//  ArchivingRepository.swift
//  PersonTabGit
//
//  Created by Joao Fernandes on 13/10/16.
//  Copyright © 2016 Joao Fernandes. All rights reserved.
//

import Foundation

class ArchivingRepository:RepositoryProtocol{
    
    //singleton repository
    static let repository = ArchivingRepository() // shared instance
    
    var documentsPath = URL(fileURLWithPath: "")
    var filePath = URL(fileURLWithPath: "")
    
    
    init(){}
    var people = [Person]()
    func savePeople() {
        documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]) // o que interessa é um array de directorias - retorna um array
        print(documentsPath)
        filePath = documentsPath.appendingPathComponent("people.data", isDirectory:false)
        
        let path = filePath.path
        
        if NSKeyedArchiver.archiveRootObject(people, toFile: path){
            print("Sucessfully saved people")
        }else {
            print("Failure saving people")
        }
        
    }
    
    func loadPeople() {
        documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]) // o que interessa é um array de directorias - retorna um array
        print(documentsPath)
        filePath = documentsPath.appendingPathComponent("people.data", isDirectory:false)
        
        let path = filePath.path
        
        if let newData = NSKeyedUnarchiver.unarchiveObject(withFile: path) as? [Person]{
            people = newData
            print("Sucessfully loaded people")
        }else {
            print("Failure loading people")
        }
 
    }

}
