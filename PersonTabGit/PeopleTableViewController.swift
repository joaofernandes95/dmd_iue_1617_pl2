//
//  PeopleTableViewController.swift
//  PersonTabGit
//
//  Created by Joao Fernandes on 07/10/16.
//  Copyright © 2016 Joao Fernandes. All rights reserved.
//

import UIKit

class PeopleTableViewController: UITableViewController {
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        

       

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     //   let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return ArchivingRepository.repository.people.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "personCell", for: indexPath) as! PersonTableViewCell
     //   let appDelegate = UIApplication.shared.delegate as! AppDelegate
        cell.person = ArchivingRepository.repository.people[indexPath.row]

        // Configure the cell...

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       // let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let person = ArchivingRepository.repository.people[indexPath.row]
        self.performSegue(withIdentifier: "editUser", sender: person)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "editUser"){
            let personViewController = segue.destination as! PersonViewController
            
            personViewController.person = sender as! Person
        
        }
    }
    
    @IBAction func addPerson(_ sender: AnyObject) {
        
        
    }
    

}
