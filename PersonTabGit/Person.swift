//
//  Person.swift
//  PersonTabGit
//
//  Created by Joao Fernandes on 29/09/16.
//  Copyright © 2016 Joao Fernandes. All rights reserved.
//

import Foundation

class Person:NSObject, NSCoding {
    var firstName:String? // ? pode ser vazio
    var lastName:String?
    var nationality:String
    
    override init() {
        self.nationality = "Portuguese"
    }
    
    init(firstname:String, lastName:String, nationality:String) {
        self.firstName = firstname
        self.lastName = lastName
        self.nationality = nationality
    }
    func encode(with aCoder: NSCoder) {
        aCoder.encode(firstName, forKey: "firstName")
        aCoder.encode(lastName, forKey: "lastName")
        aCoder.encode(nationality, forKey: "nationality")
    }
    required init?(coder aDecoder: NSCoder) {
        firstName = aDecoder.decodeObject(forKey: "firstName") as? String
        lastName = aDecoder.decodeObject(forKey: "lastName") as? String
        let nat = aDecoder.decodeObject(forKey: "nationality") as? String
        
        if let n = nat {
            nationality = n
        } else {
            nationality = "Portuguese"
        }
    }
    
    func fullName() -> String {
        
        if let first = firstName {
            firstName = first
        }
        if let last = lastName {
            lastName = last
        }
        return firstName! + " " + lastName!
    }


}
