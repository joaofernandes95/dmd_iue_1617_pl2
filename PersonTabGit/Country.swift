//
//  Country.swift
//  PersonTabGit
//
//  Created by Joao Fernandes on 17/10/16.
//  Copyright © 2016 Joao Fernandes. All rights reserved.
//

import Foundation

struct Country {
    let name:String
    let population:Int
    let capital:String
    
}

