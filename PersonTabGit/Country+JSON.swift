//
//  Country+JSON.swift
//  PersonTabGit
//
//  Created by Joao Fernandes on 17/10/16.
//  Copyright © 2016 Joao Fernandes. All rights reserved.
//

import Foundation

extension Country{

    static func parse(json:[String:Any]) -> Country? {
        guard let name = json["countryName"] as? String else {
            return nil
        }
        
        guard let capital = json["capital"] as? String else {
            return nil
        }
        
        guard let populationStr = json["population"] as? String else {
            return nil
        }
        let population = Int(populationStr) ?? 0
        let country = Country(name: name, population: population, capital: capital)
        
        
        return country
        
        
        
    
    }

}
