//
//  RepositoryProtocol.swift
//  PersonTabGit
//
//  Created by Joao Fernandes on 13/10/16.
//  Copyright © 2016 Joao Fernandes. All rights reserved.
//

import Foundation

protocol RepositoryProtocol {
    func savePeople()
    func loadPeople()

}
