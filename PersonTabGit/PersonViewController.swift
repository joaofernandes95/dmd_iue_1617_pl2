//
//  PersonViewController.swift
//  PersonTabGit
//
//  Created by Joao Fernandes on 29/09/16.
//  Copyright © 2016 Joao Fernandes. All rights reserved.
//

import UIKit
import Social

class PersonViewController: UIViewController {
    
    @IBOutlet weak var labelText: UILabel!

    @IBOutlet weak var firstNameText: UITextField!
    
    @IBOutlet weak var lastNameText: UITextField!
    
    @IBOutlet weak var nationalityText: UITextField!
    
    @IBOutlet weak var textToPersist: UITextField!
    
    
    var person:Person?
    
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.firstNameText.text = self.person?.firstName
        self.lastNameText.text = self.person?.lastName
        self.nationalityText.text = self.person?.nationality

        if(person == nil){
            
            //criar um botão cancel caso eu queira criar uma pessoa nova
            let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.cancelAction))
            self.navigationItem.leftBarButtonItem = cancelButton
        
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        defaults.set(textToPersist.text, forKey: "text")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        textToPersist.text = defaults.string(forKey: "text")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveAction(_ sender: AnyObject) {
        if let p = person {
            //p existes e é pessoa
            p.firstName = firstNameText.text
            p.lastName = lastNameText.text
            p.nationality = nationalityText.text!
            
            _ = navigationController?.popViewController(animated: true)
        }
        else {
            
            //Create new person
            let newPerson =  Person(firstname: firstNameText.text!, lastName: lastNameText.text!, nationality: nationalityText.text!)
            
          // let appDelegate = UIApplication.shared.delegate as! AppDelegate
            ArchivingRepository.repository.people.append(newPerson)
            ArchivingRepository.repository.savePeople()
            self.navigationController?.dismiss(animated: true, completion: nil)
        
        }
    }
    @IBAction func setButtonPressed(_ sender: AnyObject) {
        
/*
        self.person.firstName = firstNameText.text
        self.person.lastName = lastNameText.text
        if let nat = nationalityText.text {
            person.nationality = nat
        }
        
        labelText.text = person.fullName() + person.nationality
        print(person.nationality)
     //   self.person.nationality = nationalityText.text!
 
 */
        
    }
    
    func cancelAction() {
        self.dismiss(animated: true, completion: {});
    
    }
    
    
    @IBAction func shareButtonPressed(_ sender: UIBarButtonItem) {
//        if firstNameText.text == "" {
//            print("Nada a partilhar")
//            return
//        }
//        else {
//            let viewController = UIActivityViewController(activityItems: [firstNameText.text], applicationActivities: nil)
//            present(viewController, animated: true, completion: nil)
//        }
        
        let twitterAction = UIAlertAction(title: "Share on Twitter", style: .default){ (action) -> Void in
            if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeTwitter){
                if let twitterVC = SLComposeViewController(forServiceType: SLServiceTypeTwitter){
                    twitterVC.setInitialText(self.firstNameText.text)
                    self.present(twitterVC, animated: true, completion: nil)
                }
            } else {
                let alert = UIAlertController(title: "Warning", message: "You must first set up a Twitter account", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        
        }
        let dismissAction = UIAlertAction(title: "Dismiss", style: .cancel, handler: nil)
        let actionSheet = UIAlertController(title: "", message: "Share person details", preferredStyle: .actionSheet)
        
        actionSheet.addAction(twitterAction)
        actionSheet.addAction(dismissAction)
        
        present(actionSheet, animated: true, completion: nil)
    }
    
    
 
    

}
